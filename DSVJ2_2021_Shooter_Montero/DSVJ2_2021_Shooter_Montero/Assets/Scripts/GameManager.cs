﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManagerInstance;
    [SerializeField] int health;
    [SerializeField] int points;
    [SerializeField] bool gameOver;

    private int bombsBroken;
    private int ghostsDestroyed;
    private int boxesDestroyed;

    private void Awake()
    {         
        if (gameManagerInstance != null)
        {
            Destroy(gameObject);
            return;          
        }
        gameManagerInstance = this;
        DontDestroyOnLoad(gameObject);

        gameManagerInstance.ResetValues();
    }
    
    public GameManager Get()
    {
        return gameManagerInstance;
    }

    public void DamagePlayer(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            gameOver = true;
        }
    }

    public void IncreaseBombsDestroyed()
    {
        bombsBroken++;
    }

    public int ReturnBombs()
    {
        return bombsBroken;
    }

    public void IncreaseGhostsDestroyed()
    {
        ghostsDestroyed++;
    }

    public int ReturnGhosts()
    {
        return ghostsDestroyed;
    }

    public void IncreaseBoxesDestroyed()
    {
        boxesDestroyed++;
    }

    public int ReturnBoxes()
    {
        return boxesDestroyed;
    }

    public void PlayerGainPoints(int objectPoints)
    {
        points += objectPoints;
    }

    public int GetPoints()
    {
        return points;
    }

    public void ResetValues()
    {
        health = 100;
        points = 0;
        gameOver = false;
    }

    public void GameOver()
    {
        gameOver = true;        
        SceneManager.LoadScene("ShooterEndGame");
    } 
}
