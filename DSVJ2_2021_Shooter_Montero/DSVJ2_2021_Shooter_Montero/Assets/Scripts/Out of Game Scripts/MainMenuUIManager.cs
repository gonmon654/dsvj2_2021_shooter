﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour
{
    [SerializeField] Button[] mainMenuButtons;

    void Update()
    {
        for (short i = 0; i < mainMenuButtons.Length; i++)
        {
            if (Input.GetMouseButtonDown(0))
            {
                switch (i)
                {
                    case 0:
                        SceneManager.LoadScene("ShooterInGame");
                        break;
                    case 1:
                        SceneManager.LoadScene("ShooterCredits");
                        break;
                    default:
                        SceneManager.LoadScene("ShooterInGame");
                        break;
                }
            }          
        }
    }
}
