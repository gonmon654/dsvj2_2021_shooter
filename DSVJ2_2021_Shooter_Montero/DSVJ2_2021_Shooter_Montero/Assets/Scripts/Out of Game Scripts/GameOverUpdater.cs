﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverUpdater : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI[] gameOverTexts;
    [SerializeField] Button restartButton;

    void Start()
    {
        gameOverTexts[0].text = "Points:" + GameManager.gameManagerInstance.GetPoints();
        gameOverTexts[1].text = "Bombs:" + GameManager.gameManagerInstance.ReturnBombs();
        gameOverTexts[2].text = "Ghosts:" + GameManager.gameManagerInstance.ReturnGhosts();
    }

    public void Click()
    {
        SceneManager.LoadScene("ShooterInGame");
        GameManager.gameManagerInstance.ResetValues();
    }

}
