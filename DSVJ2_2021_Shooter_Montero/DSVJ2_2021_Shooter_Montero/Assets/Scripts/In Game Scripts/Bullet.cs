﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Bomb":
                GameManager.gameManagerInstance.PlayerGainPoints(50);
                collision.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(1000);
                break;
            case "Ghost":
                GameManager.gameManagerInstance.PlayerGainPoints(100);
                collision.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(1000);
                break;
            case "Box":
                GameManager.gameManagerInstance.PlayerGainPoints(100);
                collision.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(1000);
                break;
            default:
                break;
        }
        Destroy(this.gameObject);
    }
}
