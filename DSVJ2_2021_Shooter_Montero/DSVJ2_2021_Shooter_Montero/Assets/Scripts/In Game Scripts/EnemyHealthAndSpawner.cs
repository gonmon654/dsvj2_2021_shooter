﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthAndSpawner : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] int health;
    [SerializeField] int pointsGiven;

    private GameObject enemyManagerAndSpawner;

    private void Start()
    {
        enemyManagerAndSpawner = GameObject.FindGameObjectWithTag("Enemy Manager");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (this.gameObject.tag != "Box")
            {
                other.gameObject.GetComponent<Player>().TakeDamage(damage);
                enemyManagerAndSpawner.GetComponent<EnemySpawnerAndManager>().EnemyDestroyed(this.gameObject);
                Destroy(this.gameObject);
            }
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            enemyManagerAndSpawner.GetComponent<EnemySpawnerAndManager>().EnemyDestroyed(this.gameObject);
            enemyManagerAndSpawner.GetComponent<EnemySpawnerAndManager>().EnemyCountIncrease(this.gameObject);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().GainPoints(pointsGiven);
            Destroy(this.gameObject);
        }
    }
}
