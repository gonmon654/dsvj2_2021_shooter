﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGuns : MonoBehaviour
{
    [SerializeField] GameObject pistol;
    [SerializeField] GameObject sniper;
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            pistol.SetActive(true);
            sniper.SetActive(false);
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            sniper.SetActive(true);
            pistol.SetActive(false);
        }
    }
}
