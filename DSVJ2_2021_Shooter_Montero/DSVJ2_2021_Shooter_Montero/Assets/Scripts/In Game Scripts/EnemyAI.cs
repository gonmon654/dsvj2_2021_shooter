﻿using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public enum EnemyState
    {
        Idle,
        FollowTarget,
        GoAway,
        Reset,
    }
    [SerializeField] private EnemyState currentState;

    [SerializeField] float speed;
    [SerializeField] float stopDistance;
    [SerializeField] float restartDistance;

    private Transform target;
    private float time;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        time += Time.deltaTime;
        switch (currentState)
        {
            case EnemyState.Idle:
                if (time > 2)
                {
                    NextState();
                }
                break;
            case EnemyState.FollowTarget:
                Vector3 targetDirection = target.position - transform.position;
                transform.Translate(targetDirection.normalized * speed * Time.deltaTime, Space.World);
                if (Vector3.Distance(transform.position, target.position) < stopDistance)
                {
                    NextState();
                }
                break;
            case EnemyState.GoAway:
                Vector3 leaveDirection = target.position - transform.position;
                transform.Translate(leaveDirection.normalized * speed * Time.deltaTime, Space.World);
                if (Vector3.Distance(transform.position, target.position) > restartDistance)
                {
                    NextState();
                }
                break;
        }
    }  

    private void NextState()
    {
        time = 0;
        int stateInt = (int)currentState;
        stateInt++;
        stateInt = stateInt % ((int)EnemyState.Reset);
        SetState((EnemyState)stateInt);
    }

    private void SetState(EnemyState es)
    {
        currentState = es;
    }
}
