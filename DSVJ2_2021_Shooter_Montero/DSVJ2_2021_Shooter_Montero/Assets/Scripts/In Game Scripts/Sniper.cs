﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject bullet;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 spawnPosition = mainCamera.transform.position + transform.forward;
            GameObject randombullet = Instantiate(bullet, spawnPosition, Quaternion.identity);
            randombullet.GetComponent<Rigidbody>().AddForce(Vector3.forward * 100, ForceMode.Impulse);
        }
    }
}
