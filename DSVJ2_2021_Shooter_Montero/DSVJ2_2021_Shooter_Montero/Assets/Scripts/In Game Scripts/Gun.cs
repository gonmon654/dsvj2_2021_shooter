﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] float range;
    [SerializeField] Camera mainCamera;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }
    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, range))
        {
            switch (hit.collider.gameObject.tag)
            {
                case "Bomb":
                    GameManager.gameManagerInstance.PlayerGainPoints(50);
                    hit.collider.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(damage);
                    break;
                case "Ghost":
                    GameManager.gameManagerInstance.PlayerGainPoints(100);
                    hit.collider.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(damage);
                    break;
                case "Box":
                    GameManager.gameManagerInstance.PlayerGainPoints(100);
                    hit.collider.gameObject.GetComponent<EnemyHealthAndSpawner>().TakeDamage(damage);
                    break;
                default:
                    break;
            }
           
        }        
    }
}
