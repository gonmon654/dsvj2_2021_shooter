﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI pointsText;
    [SerializeField] TextMeshProUGUI healthText;

    private int health;
    private int points;
   
    void Start()
    {
        health = 100;
        points = 0;
    }

    public void GainPoints(int objectPoints)
    {
        points += objectPoints;
        GameManager.gameManagerInstance.PlayerGainPoints(objectPoints);
        pointsText.text = "Points: " + points;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        GameManager.gameManagerInstance.DamagePlayer(damage);
        healthText.text = "Health: " + health;
        if (health <= 0)
        {
            GameManager.gameManagerInstance.GameOver();
        }
    }
}
