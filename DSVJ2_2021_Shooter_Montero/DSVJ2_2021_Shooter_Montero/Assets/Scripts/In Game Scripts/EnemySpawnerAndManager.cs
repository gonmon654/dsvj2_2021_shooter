﻿using UnityEngine;

public class EnemySpawnerAndManager : MonoBehaviour
{
    [SerializeField] GameObject player;

    [Header("Enemy Related")]
    [SerializeField] GameObject bombPrefab;
    [SerializeField] GameObject ghostPrefab;
    [SerializeField] GameObject boxPrefab;

    [SerializeField] float spawnRange;

    [SerializeField] int maxBombAmount;
    [SerializeField] int maxGhostAmount;
    [SerializeField] int maxBoxAmount;

    [SerializeField] float bombSpawnTimer;
    [SerializeField] float ghostSpawnTimer;
    [SerializeField] float boxSpawnTimer;

    private float lastSpawnBomb;
    private float lastSpawnGhost;
    private float lastSpawnBox;

    private int currentBombAmount;
    private int currentGhostAmount;
    private int currentBoxAmount;

    void Start()
    {
        for (int i = 0; i < maxBombAmount; i++)
        {
            CreateBomb();            
        }
        for (int i = 0; i < maxGhostAmount; i++)
        {
            CreateGhost();
        }
        for (int i = 0; i < maxBoxAmount; i++)
        {
            CreateBox();
        }
    }
    
    void Update()
    {
        lastSpawnBomb -= Time.deltaTime;
        lastSpawnGhost -= Time.deltaTime;
        lastSpawnBox -= Time.deltaTime;
        if (lastSpawnBomb < 0 && currentBombAmount < maxBombAmount)
        {
            CreateBomb();
        }
        if (lastSpawnGhost < 0 && currentGhostAmount < maxGhostAmount)
        {
            CreateGhost();
        }
        if (lastSpawnBox < 0 && currentBoxAmount < maxBoxAmount)
        {
            CreateBox();
        }
    }

    void CreateBomb()
    {
        GameObject bombGameObject = Instantiate(bombPrefab);
        bombGameObject.transform.parent = this.transform;
        Vector3 bombPosition = Random.insideUnitCircle * spawnRange;
        bombPosition.z = bombPosition.y;
        bombPosition.y = 0 + (bombPrefab.GetComponent<SphereCollider>().radius / 2);
        bombGameObject.transform.localPosition = bombPosition;
        lastSpawnBomb = bombSpawnTimer;
        currentBombAmount++;
    }

    void CreateGhost()
    {
        GameObject ghostGameObject = Instantiate(ghostPrefab);
        ghostGameObject.transform.parent = this.transform;
        Vector3 ghostPosition = Random.insideUnitCircle * spawnRange;
        ghostPosition.z = ghostPosition.y;
        ghostPosition.y = 0 + (ghostPrefab.GetComponent<CapsuleCollider>().radius / 2);
        ghostGameObject.transform.localPosition = ghostPosition;
        lastSpawnGhost = ghostSpawnTimer;
        currentGhostAmount++;
    }

    void CreateBox()
    {
        GameObject boxGameObject = Instantiate(boxPrefab);
        boxGameObject.transform.parent = this.transform;
        Vector3 boxPosition = Random.insideUnitCircle * spawnRange;
        boxPosition.z = boxPosition.y;
        boxPosition.y = 0;
        boxGameObject.transform.localPosition = boxPosition;
        lastSpawnBox = boxSpawnTimer;
        currentBoxAmount++;
    }

    public void EnemyDestroyed(GameObject enemy)
    {
        if (enemy.tag == "Bomb")
        {
            currentBombAmount--;
        }
        else if (enemy.tag == "Box")
        {
            currentBoxAmount--;
        }
        else if (enemy.tag == "Ghost")
        {
            currentGhostAmount--;
        }
    }

    public void EnemyCountIncrease(GameObject enemy)
    {
        if (enemy.tag == "Bomb")
        {
            GameManager.gameManagerInstance.IncreaseBombsDestroyed();
        }
        else if (enemy.tag == "Box")
        {
            GameManager.gameManagerInstance.IncreaseBoxesDestroyed();
        }
        else if (enemy.tag == "Ghost")
        {
            GameManager.gameManagerInstance.IncreaseGhostsDestroyed();
        }
    }
}
